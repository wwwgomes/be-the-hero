const connection = require('../database/connection');

module.exports = {
  async index(req, res) {
    const { page = 1 } = req.query;

    const [ count ] = await connection('incidents').count();

    const results = await connection('incidents')
      .join('ongs', 'ongs.id', '=', 'incidents.ong_id')
      .limit(5)
      .offset((page - 1) * 5)
      .select([
        'incidents.*', 
        'ongs.name', 
        'ongs.email', 
        'ongs.whatsapp', 
        'ongs.city', 
        'ongs.uf'
      ]);
      
    let incidents = [];
    for (const result in results) {
      const ong = {
        ong_id: results[result].ong_id,
        name: results[result].name,
        email: results[result].email,
        whatsapp: results[result].whatsapp,
        city: results[result].city,
        uf: results[result].uf,
      };
      const incident = {
        id: results[result].id,
        title: results[result].title,
        description: results[result].description,
        value: results[result].value,
        ong,
      }

      incidents.push(incident);
    }
    
    res.header('X-Total-Count', count['count(*)']);

    return res.json(incidents);
  },
  async create(req, res) {
    const { title, description, value } = req.body;
    const ong_id = req.headers.authorization;

    const [id] = await connection('incidents').insert({
      title,
      description,
      value,
      ong_id,
    });

    return res.json({ id });
  },
  async delete(request, response) {
    const { id } = request.params;
    const ong_id = request.headers.authorization;

    const incident = await connection('incidents')
      .where('id', id)
      .select('ong_id')
      .first();

    if (incident.ong_id !== ong_id) {
      return response.status(401).json({ error: 'Operation not permitted.' });
    }

    await connection('incidents').where('id', id).delete();

    return response.status(204).send();
  }
}